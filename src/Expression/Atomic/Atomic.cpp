#include "Symbolic/Expression/Atomic/Atomic.hpp"

#include "Symbolic/Expression/Atomic/Exact.hpp"

ExpressionPtr Atomic::simplify() const {
    return to_ptr();
}
ExpressionPtr Atomic::expand() const {
    return to_ptr();
}
ExpressionPtr Atomic::substitute(__attribute__((unused)) const Expression &sel, __attribute__((unused)) const Expression &subst) const {
    return to_ptr();
}
ExpressionPtr Atomic::differentiate(__attribute__((unused)) const Expression &d) const {
    return Exact(0).to_ptr();
}

std::string Atomic::to_string(bool latex) const {
    return latex ? latex_symbol : symbol;
}
std::string Atomic::to_export_string() const {
    return name + "(" + symbol + ")";
}