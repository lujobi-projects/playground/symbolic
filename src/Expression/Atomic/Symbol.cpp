#include "Symbolic/Expression/Atomic/Symbol.hpp"

#include "Symbolic/Expression/Atomic/Exact.hpp"

ExpressionPtr Symbol::substitute(const Expression &sel, const Expression &subst) const {
    if (equals(sel, true)) {
        return subst.to_ptr();
    }
    return to_ptr();
}
ExpressionPtr Symbol::differentiate(__attribute__((unused)) const Expression &d) const {
    if (equals(d, true)) {
        return Exact(1).to_ptr();
    }
    return Exact(0).to_ptr();
}

bool Symbol::equals(const Expression &other, __attribute__((unused)) bool exact) const {
    SymbolPtr sym = other.cast_ptr<Symbol>();
    return sym && sym->symbol == symbol;
}

ExpressionPtr Symbol::to_ptr() const {
    return std::make_shared<Symbol>(symbol);
}
