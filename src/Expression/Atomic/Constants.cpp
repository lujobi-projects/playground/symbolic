#include "Symbolic/Expression/Atomic/Constant.hpp"

bool Constant::equals(const Expression &other, __attribute__((unused)) bool exact) const {
    return other.isType<Constant>() && other.cast_ptr<Constant>()->latex_symbol == latex_symbol;
}

ExpressionPtr Constant::to_ptr() const {
    return std::make_shared<Constant>(symbol, latex_symbol);
}
