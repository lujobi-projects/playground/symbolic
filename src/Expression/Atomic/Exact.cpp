#include "Symbolic/Expression/Atomic/Exact.hpp"

bool Exact::equals(const Expression &other, __attribute__((unused)) bool exact) const {
    ExactPtr ex = other.cast_ptr<Exact>();
    return other.isType<Exact>() && ex->no == no;
}

ExpressionPtr Exact::to_ptr() const {
    return std::make_shared<Exact>(no);
}
