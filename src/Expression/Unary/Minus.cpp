#include "Symbolic/Expression/Unary/Minus.hpp"

ExpressionPtr Minus::simplify() const {
    return Minus(*expr->simplify()).to_ptr();
}
ExpressionPtr Minus::expand() const {
    return Minus(*expr->expand()).to_ptr();
}
ExpressionPtr Minus::substitute(const Expression &sel, const Expression &subst) const {
    return Minus(*expr->substitute(sel, subst)).to_ptr();
}
ExpressionPtr Minus::differentiate(const Expression &d) const {
    return Minus(*expr->differentiate(d)).to_ptr();
}

#include <iostream>

bool Minus::equals(const Expression &other, bool exact) const {
    MinusPtr ptr = other.cast_ptr<Minus>();
    std::cout << "22" << std::endl;
    if (exact) {
        std::cout << other.isType<Minus>() << (other.isType<Minus>() && expr->equals(*ptr->expr, exact)) << expr->to_string() << ptr->to_string() << std::endl;
        if (other.isType<Minus>()) {
            std::cout << ptr->expr->to_export_string() << std::endl;
        }
        return other.isType<Minus>() && expr->equals(*ptr->expr, exact);
    }
    // std::cout << "33333" << std::endl;
    if (other.isType<Exact>() && expr->isType<Exact>()) {
        ExactPtr o_ptr = other.cast_ptr<Exact>();
        ExactPtr expr_ptr = expr->cast_ptr<Exact>();
        return o_ptr->no == -expr_ptr->no;
    }
    std::cout << "jkjldjljflkjfgdlkj" << std::endl;
    return other.isType<Minus>() && expr->equals(*ptr->expr, exact);
}

ExpressionPtr Minus::to_ptr() const {
    return std::make_shared<Minus>(*this);
}
