#include "Symbolic/Expression/Unary/UnaryExpression.hpp"

std::string UnaryExpression::to_string(bool latex) const {
    bool br = this->precedence < expr->precedence;
    return symbol + (br ? "(" : "") + expr->to_string(latex) + (br ? ")" : "");
}

std::string UnaryExpression::to_export_string() const {
    return name + "(" + expr->to_export_string() + ")";
}