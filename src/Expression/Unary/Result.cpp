#include "Symbolic/Expression/Unary/Result.hpp"

ExpressionPtr Result::simplify() const {
    return Result(*expr->simplify()).to_ptr();
}
ExpressionPtr Result::expand() const {
    return Result(*expr->expand()).to_ptr();
}
ExpressionPtr Result::substitute(const Expression &sel, const Expression &subst) const {
    return Result(*expr->substitute(sel, subst)).to_ptr();
}
ExpressionPtr Result::differentiate(const Expression &d) const {
    return Result(*expr->differentiate(d)).to_ptr();
}

bool Result::equals(const Expression &other, bool exact) const {
    if (exact) {
        return other.isType<Result>() && expr->equals(other.extract_result(), exact);
    }
    return expr->equals(other.extract_result(), exact);
}

ExpressionPtr Result::to_ptr() const {
    return std::make_shared<Result>(*this);
}

const Expression &Result::extract_result() const {
    return *expr;
}

// Result &Result::operator=(ExpressionPtr e) {

//     return *this;
// }