#include "Symbolic/Expression/Binary/Subtraction.hpp"

#include "Symbolic/Expression/Atomic/Exact.hpp"
#include "Symbolic/Expression/Operators.hpp"
#include "Symbolic/Expression/Unary/Minus.hpp"

ExpressionPtr Subtraction::simplify() const {
    if (left->extract_result().isType<Exact>() && right->extract_result().isType<Exact>()) {
        unsigned int left_no = left->cast_ptr<Exact>()->no;
        unsigned int right_no = right->cast_ptr<Exact>()->no;
        if (left_no >= right_no) {
            return Exact(left_no - right_no).to_ptr();
        }
        return Minus(Exact(right_no - left_no)).to_ptr();
    } else if (*left == Exact(0)) {
        return Minus(*right).to_ptr();
    } else if (*right == Exact(0)) {
        return left->to_ptr();
    }
    return Subtraction(*left->simplify()->to_ptr(), *right->simplify()->to_ptr()).to_ptr();
}
ExpressionPtr Subtraction::expand() const {
    return to_ptr();
}
ExpressionPtr Subtraction::substitute(const Expression &sel, const Expression &subst) const {
    return std::make_shared<Subtraction>(*left->substitute(sel, subst), *right->substitute(sel, subst));
}
ExpressionPtr Subtraction::differentiate(const Expression &d) const {
    return std::make_shared<Subtraction>(*left->differentiate(d), *right->differentiate(d));
}
bool Subtraction::equals(const Expression &other, bool exact) const {
    SubtractionPtr add = other.cast_ptr<Subtraction>();
    if (exact) {
        return other.isType<Subtraction>() && left->equals(*add->left, exact) && right->equals(*add->right, exact);
    }
    add = other.extract_result().cast_ptr<Subtraction>();
    return other.extract_result().isType<Subtraction>() && left->equals(*add->left, exact) && right->equals(*add->right, exact);
}

ExpressionPtr Subtraction::to_ptr() const {
    return std::make_shared<Subtraction>(*left, *right);
}
