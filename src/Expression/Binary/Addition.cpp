#include "Symbolic/Expression/Binary/Addition.hpp"

#include "Symbolic/Expression/Atomic/Exact.hpp"
#include "Symbolic/Expression/Operators.hpp"

ExpressionPtr Addition::simplify() const {
    if (left->extract_result().isType<Exact>() && right->extract_result().isType<Exact>()) {
        ExactPtr left_ptr = left->cast_ptr<Exact>();
        ExactPtr right_ptr = right->cast_ptr<Exact>();
        return Exact(left_ptr->no + right_ptr->no).to_ptr();
    } else if (*left == Exact(0)) {
        return right->to_ptr();
    } else if (*right == Exact(0)) {
        return left->to_ptr();
    }
    return Addition(*left->simplify()->to_ptr(), *right->simplify()->to_ptr()).to_ptr();
}
ExpressionPtr Addition::expand() const {
    return to_ptr();
}
ExpressionPtr Addition::substitute(const Expression &sel, const Expression &subst) const {
    return std::make_shared<Addition>(*left->substitute(sel, subst), *right->substitute(sel, subst));
}
ExpressionPtr Addition::differentiate(const Expression &d) const {
    return std::make_shared<Addition>(*left->differentiate(d), *right->differentiate(d));
}
bool Addition::equals(const Expression &other, bool exact) const {
    AdditionPtr add = other.cast_ptr<Addition>();
    if (exact) {
        return other.isType<Addition>() && left->equals(*add->left, exact) && right->equals(*add->right, exact);
    }
    add = other.extract_result().cast_ptr<Addition>();
    return other.extract_result().isType<Addition>() && left->equals(*add->left, exact) && right->equals(*add->right, exact);
}

ExpressionPtr Addition::to_ptr() const {
    return std::make_shared<Addition>(*left, *right);
}
