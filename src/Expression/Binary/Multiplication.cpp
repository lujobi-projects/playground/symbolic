#include "Symbolic/Expression/Binary/Multiplication.hpp"

#include "Symbolic/Expression/Atomic/Exact.hpp"
#include "Symbolic/Expression/Binary/Addition.hpp"
#include "Symbolic/Expression/Operators.hpp"

ExpressionPtr Multiplication::simplify() const {
    if (left->extract_result().isType<Exact>() && right->extract_result().isType<Exact>()) {
        ExactPtr left_ptr = left->cast_ptr<Exact>();
        ExactPtr right_ptr = right->cast_ptr<Exact>();
        return Exact(left_ptr->no * right_ptr->no).to_ptr();
    } else if (*left == Exact(0) || *right == Exact(0)) {
        return Exact(0).to_ptr();
    } else if (*left == Exact(1)) {
        return right->simplify()->to_ptr();
    } else if (*right == Exact(1)) {
        return left->simplify()->to_ptr();
    }
    return Multiplication(*left->simplify()->to_ptr(), *right->simplify()->to_ptr()).to_ptr();
}
ExpressionPtr Multiplication::expand() const {
    return to_ptr();
}
ExpressionPtr Multiplication::substitute(const Expression &sel, const Expression &subst) const {
    return std::make_shared<Multiplication>(*left->substitute(sel, subst), *right->substitute(sel, subst));
}
ExpressionPtr Multiplication::differentiate(const Expression &d) const {
    Multiplication mul_left(*left->differentiate(d), *right);
    Multiplication mul_right(*left, *right->differentiate(d));
    Addition res(mul_left, mul_right);
    return std::make_shared<Addition>(res);
}

bool Multiplication::equals(const Expression &other, bool exact) const {
    MultiplicationPtr add = other.cast_ptr<Multiplication>();
    if (exact) {
        return other.isType<Multiplication>() && left->equals(*add->left, exact) && right->equals(*add->right, exact);
    }
    add = other.extract_result().cast_ptr<Multiplication>();
    return other.extract_result().isType<Multiplication>() && left->equals(*add->left, exact) && right->equals(*add->right, exact);
}

ExpressionPtr Multiplication::to_ptr() const {
    return std::make_shared<Multiplication>(*left, *right);
}
