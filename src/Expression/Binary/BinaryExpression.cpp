#include "Symbolic/Expression/Binary/BinaryExpression.hpp"

std::string BinaryExpression::to_string(bool latex) const {
    bool l_br = this->precedence < left->precedence;
    bool r_br = this->precedence < right->precedence;
    return (l_br ? "(" : "") + left->to_string(latex) + (l_br ? ") " : " ") +
           symbol +
           (r_br ? " (" : " ") + right->to_string(latex) + (r_br ? ")" : "");
}

std::string BinaryExpression::to_export_string() const {
    return name + "(" + left->to_export_string() + ", " + right->to_export_string() + ")";
}