#include "Symbolic/Expression/Operators.hpp"

Result operator+(const Expression &e_left, const Expression &e_right) {
    return Result(Addition(e_left, e_right));
}
Result operator-(const Expression &e_left, const Expression &e_right) {
    return Result(Subtraction(e_left, e_right));
}

Result operator-(const Expression &exp) {
    return Result(Minus(exp));
}

Result operator*(const Expression &e_left, const Expression &e_right) {
    return Result(Multiplication(e_left, e_right));
}

bool operator==(const Expression &e_left, const Expression &e_right) {
    return e_left.equals(e_right);
}
bool operator!=(const Expression &e_left, const Expression &e_right) {
    return !e_left.equals(e_right);
}

Result diffenentiate(const Expression &exp, const Expression &diff) {
    return Result(*exp.differentiate(diff));
}
Result simplify(const Expression &exp) {
    ExpressionPtr res = exp.extract_result().simplify();
    if (res->extract_result().equals(exp.extract_result(), true)) {
        return Result(*res);
    }
    return simplify(*res);
}

Result substitute(const Expression &exp, const Expression &sel, const Expression &subst) {
    return Result(*exp.substitute(sel, subst));
}

std::ostream &operator<<(std::ostream &out, const Expression &e_right) {
    return out << e_right.to_string(false);
}