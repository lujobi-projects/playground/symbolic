#include "Symbolic/Expression/Advanced/Sum.hpp"

#include <iostream>

std::vector<ExpressionPtr> adjust(std::vector<ExpressionPtr> v_in) {
    std::vector<ExpressionPtr> v_out;
    v_out.resize(v_in.size());
    std::transform<>(v_in.begin(), v_in.end(), v_out.begin(),
                     [](ExpressionPtr ptr) -> ExpressionPtr { return (ptr->extract_result()).to_ptr(); });
    return v_out;
}

std::string Sum::to_string(bool latex) const {
    if (expressions.size() == 0) return "";
    std::string s = "";
    for (unsigned int i = 0; i + 1 < expressions.size(); i++) {
        if (this->precedence < expressions.at(i)->precedence)
            s += "(" + expressions.at(i)->to_string(latex) + ")";
        else
            s += expressions.at(i)->to_string(latex);
        s += " " + (latex ? latex_symbol : symbol) + " ";
    }
    if (this->precedence < expressions.at(expressions.size() - 1)->precedence)
        s += "(" + expressions.at(expressions.size() - 1)->to_string(latex) + ")";
    else
        s += expressions.at(expressions.size() - 1)->to_string(latex);
    return s;
}

std::string Sum::to_export_string() const {
    if (expressions.size() == 0) return name + "()";
    std::string s = name + "(";
    for (unsigned int i = 0; i + 1 < expressions.size(); i++) {
        s += expressions.at(i)->to_export_string() + ", ";
    }
    s += expressions.at(expressions.size() - 1)->to_export_string() + ")";
    return s;
}

ExpressionPtr Sum::simplify() const {
    return nullptr;
}
ExpressionPtr Sum::expand() const {
    return nullptr;
}
ExpressionPtr Sum::substitute(const Expression &sel, const Expression &subst) const {
    return nullptr;
}
ExpressionPtr Sum::differentiate(const Expression &d) const {
    return nullptr;
}
bool Sum::equals(const Expression &other, bool exact) const {
    return false;
    // SumPtr add = other.cast_ptr<Sum>();
    // if (exact) {
    //     return other.isType<Sum>() && left->equals(*add->left, exact) && right->equals(*add->right, exact);
    // }
    // add = other.extract_result().cast_ptr<Sum>();
    // return other.extract_result().isType<Sum>() && left->equals(*add->left, exact) && right->equals(*add->right, exact);
}

ExpressionPtr Sum::to_ptr() const {
    return std::make_shared<Sum>(expressions);
}
