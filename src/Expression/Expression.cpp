#include "Symbolic/Expression/Expression.hpp"

Expression::Expression(unsigned int precedence_, std::string symbol_, std::string latex_symbol_, std::string name_)
    : precedence(precedence_), symbol(symbol_), name(name_), latex_symbol(latex_symbol_) {}

const Expression &Expression::extract_result() const {
    return *this;
}
