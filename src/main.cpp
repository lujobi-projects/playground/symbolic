#include <iostream>
#include <string>

#include "Symbolic/Symbolic.hpp"

int main() {
    // Exact e0(0);
    // Exact e1(1);
    // Symbol x("x");
    // Symbol x1("x");
    // Symbol y("y");

    ExpressionPtr ptr;
    Exact e(20);
    Symbol s("ueli");
    Symbol w("peter");
    Result res = s + w;
    Result x = substitute(res, s, w);
    std::cout << res << std::endl;
    std::cout << x << std::endl;
    std::cout << diffenentiate(Multiplication(s, w), w) << std::endl;
    std::cout << diffenentiate(x, w) << std::endl;

    // Exact n1(1);
    // std::cout << n1 + n1 << std::endl;
    // Exact no = Exact(1000);
    // Exact n(20);
    // Exact b(20);

    // Addition a(n, n);
    // int x_1235423 = 100;
    // std::cout << x_1235423 << std::endl;
    // // std::cout << Precedence::ADDITION << std::endl;
    // std::cout << a << std::endl;
    // std::cout << a + n << std::endl;

    // Result ueli = n + a;
    // std::cout << ueli.to_export_string() << "  ueli" << std::endl;
    // Result ueli2 = ueli + n;
    // std::cout << ueli2.to_export_string() << "  ueli2" << std::endl;
    // Result ueli3 = n + a + b;
    // std::cout << (n + a + b).to_export_string() << "  n+a" << std::endl;

    // std::cout << ueli3.to_export_string() << "  ueli3" << std::endl;

    // std::cout << (n + a).to_export_string() << std::endl;
}