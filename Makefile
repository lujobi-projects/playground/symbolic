build:
	cmake -S . -B build
	cmake --build build
	
build-debug:
	cmake -S . -B build -DCMAKE_BUILD_TYPE=Debug
	cmake --build build --config Debug

test: build-debug
	cd build && ctest

remove:
	rm -r build coverage || true

run: build
	cd build && ./main

coverage: build-debug test
	rm -rf coverage
	mkdir -p coverage
	lcov -c --directory build/CMakeFiles/symbolic_test.dir --output-file coverage/coverage.info
	lcov --remove coverage/coverage.info '/usr/*' '*/_deps/*' '*/test/*' --output-file coverage/coverage.info
	

coverage-html: coverage
	cd coverage; genhtml coverage.info --output-directory out

all: remove coverage-html