#pragma once

#include "Symbolic/Expression/Expression.hpp"

class UnaryExpression : public Expression {
   public:
    UnaryExpression(const Expression& expr_,
                    unsigned int precedence_ = 0,
                    std::string symbol_ = "",
                    std::string latex_symbol_ = "",
                    std::string name_ = "UnaryExpression")
        : Expression(precedence_, symbol_, latex_symbol_, name_),
          expr(expr_.extract_result().to_ptr()){};

    virtual std::string to_string(bool latex = false) const override;
    virtual std::string to_export_string() const override;

   protected:
    const ExpressionPtr expr;
};