#pragma once

#include "UnaryExpression.hpp"

class Result : public UnaryExpression {
   public:
    explicit Result(const Expression &res) : UnaryExpression(res, Precedence::RESULT, "", "", "Result"){};
    Result(const Result &other) : Result(*(other.expr)){};
    // Result &operator=(ExpressionPtr e);

    virtual ExpressionPtr simplify() const override;
    virtual ExpressionPtr expand() const override;
    virtual ExpressionPtr substitute(const Expression &sel, const Expression &subst) const override;
    virtual ExpressionPtr differentiate(const Expression &d) const override;

    virtual bool equals(const Expression &other, bool exact = false) const;

    virtual ExpressionPtr to_ptr() const override;

    virtual const Expression &extract_result() const;
};

typedef std::shared_ptr<Result> ResultPtr;