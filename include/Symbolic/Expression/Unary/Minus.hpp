#pragma once

#include "Symbolic/Expression/Atomic/Exact.hpp"
#include "UnaryExpression.hpp"

class Minus : public UnaryExpression {
   public:
    explicit Minus(const Expression &res) : UnaryExpression(res, Precedence::UNARY, "-", "-", "Minus"){};
    Minus(const Minus &other) : Minus(*(other.expr)){};

    virtual ExpressionPtr simplify() const override;
    virtual ExpressionPtr expand() const override;
    virtual ExpressionPtr substitute(const Expression &sel, const Expression &subst) const override;
    virtual ExpressionPtr differentiate(const Expression &d) const override;

    virtual bool equals(const Expression &other, bool exact = false) const;

    virtual ExpressionPtr to_ptr() const override;
};

typedef std::shared_ptr<Minus> MinusPtr;