#pragma once

#include "Symbolic/Expression/Expression.hpp"

class Atomic : public Expression {
   public:
    using Expression::Expression;

    virtual ExpressionPtr simplify() const override;
    virtual ExpressionPtr expand() const override;
    virtual ExpressionPtr substitute(const Expression &sel, const Expression &subst) const override;
    virtual ExpressionPtr differentiate(const Expression &d) const override;

    virtual std::string to_string(bool latex = false) const override;
    virtual std::string to_export_string() const override;
};