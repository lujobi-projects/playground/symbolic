#pragma once

#include "Atomic.hpp"

class Symbol : public Atomic {
   public:
    explicit Symbol(std::string symb_name_) : Atomic(Precedence::ATOMIC, symb_name_, symb_name_, "Symbol"){};
    Symbol(const Symbol &other) : Symbol(other.symbol){};

    virtual ExpressionPtr substitute(const Expression &sel, const Expression &subst) const override;
    virtual ExpressionPtr differentiate(const Expression &d) const override;

    virtual bool equals(const Expression &other, bool exact = false) const;
    virtual ExpressionPtr to_ptr() const override;
};

typedef std::shared_ptr<Symbol> SymbolPtr;