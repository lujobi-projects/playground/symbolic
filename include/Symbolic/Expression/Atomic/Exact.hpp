#pragma once

#include "Atomic.hpp"

class Exact : public Atomic {
   public:
    explicit Exact(unsigned int n) : Atomic(Precedence::ATOMIC, std::to_string(n), std::to_string(n), "Exact"), no(n){};
    Exact(const Exact &other) : Exact(other.no){};

    virtual bool equals(const Expression &other, bool exact = false) const override;
    virtual ExpressionPtr to_ptr() const override;

    const unsigned int no;
};

typedef std::shared_ptr<Exact> ExactPtr;