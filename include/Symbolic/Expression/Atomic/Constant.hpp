#pragma once

#include "Atomic.hpp"

class Constant : public Atomic {
   public:
    Constant(std::string sign_, std::string latex_sign_)
        : Atomic(Precedence::ATOMIC, sign_, latex_sign_, "Constant"){};
    Constant(const Constant &other) : Constant(other.symbol, other.latex_symbol){};

    virtual bool equals(const Expression &other, bool exact = false) const;
    virtual ExpressionPtr to_ptr() const override;
};

const Constant PI("pi", "\\pi");
const Constant E("e", "\\e");

typedef std::shared_ptr<Constant> ConstantPtr;