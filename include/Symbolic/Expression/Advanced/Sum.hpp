#pragma once

#include <algorithm>
#include <vector>

#include "Symbolic/Expression/Expression.hpp"

std::vector<ExpressionPtr> adjust(std::vector<ExpressionPtr> v_in);

class Sum : public Expression {
   public:
    Sum(std::vector<ExpressionPtr> expressions_)
        : Expression(Precedence::ADDITION, "+", "+", "Sum"),
          expressions(adjust(expressions_)){};

    virtual std::string to_string(bool latex = false) const override;
    virtual std::string to_export_string() const override;

    virtual ExpressionPtr simplify() const;
    virtual ExpressionPtr expand() const;
    virtual ExpressionPtr substitute(const Expression &sel, const Expression &subst) const;
    virtual ExpressionPtr differentiate(const Expression &d) const;

    virtual bool equals(const Expression &other, bool exact = false) const;

    virtual ExpressionPtr to_ptr() const override;

    const std::vector<ExpressionPtr> expressions;
};

typedef std::shared_ptr<Sum> SumPtr;