#pragma once

#include <memory>
#include <string>

#include "Symbolic/Precedences.hpp"

class Expression;

typedef std::shared_ptr<Expression> ExpressionPtr;

class Expression {
   public:
    Expression(unsigned int precedence_ = 0, std::string symbol_ = "", std::string latex_symbol_ = "", std::string name_ = "Expression");
    Expression() = default;

    virtual ExpressionPtr simplify() const = 0;
    virtual ExpressionPtr expand() const = 0;
    virtual ExpressionPtr substitute(const Expression &sel, const Expression &subst) const = 0;
    virtual ExpressionPtr differentiate(const Expression &d) const = 0;

    virtual bool equals(const Expression &other, bool exact = false) const = 0;

    virtual std::string to_string(bool latex = false) const = 0;
    virtual std::string to_export_string() const = 0;
    virtual ExpressionPtr to_ptr() const = 0;

    virtual ~Expression() = default;

    template <typename T>
    bool isType() const {
        return dynamic_cast<const T *>(this) != NULL;
    }

    template <typename T>
    std::shared_ptr<T> cast_ptr() const {
        return std::static_pointer_cast<T>(this->to_ptr());
    }

    virtual const Expression &extract_result() const;

    const unsigned int precedence = 0;
    const std::string symbol = "";
    const std::string name = "Expression";
    const std::string latex_symbol = "";
};
