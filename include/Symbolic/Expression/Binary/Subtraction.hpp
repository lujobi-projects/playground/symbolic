#pragma once

#include "BinaryExpression.hpp"

class Subtraction : public BinaryExpression {
   public:
    explicit Subtraction(const Expression &left_, const Expression &right_)
        : BinaryExpression(left_, right_, Precedence::SUBTRACTION, "-", "-", "Subtraction"){};
    Subtraction(const Subtraction &other) : Subtraction(*(other.left), *(other.right)){};

    virtual ExpressionPtr simplify() const;
    virtual ExpressionPtr expand() const;
    virtual ExpressionPtr substitute(const Expression &sel, const Expression &subst) const;
    virtual ExpressionPtr differentiate(const Expression &d) const;

    virtual bool equals(const Expression &other, bool exact = false) const;

    virtual ExpressionPtr to_ptr() const override;

   private:
    double no = 20;
};

typedef std::shared_ptr<Subtraction> SubtractionPtr;