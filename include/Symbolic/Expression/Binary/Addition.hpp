#pragma once

#include "BinaryExpression.hpp"

class Addition : public BinaryExpression {
   public:
    explicit Addition(const Expression &left_, const Expression &right_)
        : BinaryExpression(left_, right_, Precedence::ADDITION, "+", "+", "Addition"){};
    Addition(const Addition &other) : Addition(*(other.left), *(other.right)){};

    virtual ExpressionPtr simplify() const;
    virtual ExpressionPtr expand() const;
    virtual ExpressionPtr substitute(const Expression &sel, const Expression &subst) const;
    virtual ExpressionPtr differentiate(const Expression &d) const;

    virtual bool equals(const Expression &other, bool exact = false) const;

    virtual ExpressionPtr to_ptr() const override;

   private:
    double no = 20;
};

typedef std::shared_ptr<Addition> AdditionPtr;