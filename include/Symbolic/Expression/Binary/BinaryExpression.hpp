#pragma once

#include "Symbolic/Expression/Expression.hpp"

class BinaryExpression : public Expression {
   public:
    BinaryExpression(const Expression& left_,
                     const Expression& right_,
                     unsigned int precedence_ = 0,
                     std::string symbol_ = "",
                     std::string latex_symbol_ = "",
                     std::string name_ = "BinaryExpression")
        : Expression(precedence_, symbol_, latex_symbol_, name_),
          left(left_.extract_result().to_ptr()),
          right(right_.extract_result().to_ptr()){};

    virtual std::string to_string(bool latex = false) const override;
    virtual std::string to_export_string() const override;

   protected:
    const ExpressionPtr left;
    const ExpressionPtr right;
};