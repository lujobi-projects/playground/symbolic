#pragma once

#include "BinaryExpression.hpp"

class Multiplication : public BinaryExpression {
   public:
    explicit Multiplication(const Expression &left_, const Expression &right_)
        : BinaryExpression(left_, right_, Precedence::MULTIPLICATION, "*", "*", "Multiplication"){};
    Multiplication(const Multiplication &other) : Multiplication(*(other.left), *(other.right)){};

    virtual ExpressionPtr simplify() const;
    virtual ExpressionPtr expand() const;
    virtual ExpressionPtr substitute(const Expression &sel, const Expression &subst) const;
    virtual ExpressionPtr differentiate(const Expression &d) const;

    virtual bool equals(const Expression &other, bool exact = false) const;

    virtual ExpressionPtr to_ptr() const override;
};
typedef std::shared_ptr<Multiplication> MultiplicationPtr;