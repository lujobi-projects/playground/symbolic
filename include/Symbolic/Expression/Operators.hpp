#include "Symbolic/Expression/Binary/Addition.hpp"
#include "Symbolic/Expression/Binary/Multiplication.hpp"
#include "Symbolic/Expression/Binary/Subtraction.hpp"
#include "Symbolic/Expression/Unary/Minus.hpp"
#include "Symbolic/Expression/Unary/Result.hpp"

Result operator+(const Expression &e_left, const Expression &e_right);
Result operator-(const Expression &e_left, const Expression &e_right);
Result operator-(const Expression &exp);
Result operator*(const Expression &e_left, const Expression &e_right);
bool operator==(const Expression &e_left, const Expression &e_right);
bool operator!=(const Expression &e_left, const Expression &e_right);
Result diffenentiate(const Expression &exp, const Expression &diff);
Result simplify(const Expression &exp);
Result substitute(const Expression &exp, const Expression &sel, const Expression &subst);
std::ostream &operator<<(std::ostream &out, const Expression &e_right);
