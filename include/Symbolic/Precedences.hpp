#pragma once

enum Precedence {
    ATOMIC = 0,
    UNARY = 1,
    POWER = 2,
    MULTIPLICATION = 3,
    DIVISION = 3,
    ADDITION = 4,
    SUBTRACTION = 4,
    RESULT = 5,
};