#include <gtest/gtest.h>

#include "Symbolic/Symbolic.hpp"

#pragma once

class ExactTest : public ::testing::Test {
   protected:
    void SetUp() override {
        e_0 = std::make_shared<Exact>(0);
        e_1 = std::make_shared<Exact>(1);
        e_20 = std::make_shared<Exact>(20);
    }

    ExactPtr e_0;
    ExactPtr e_1;
    ExactPtr e_20;
};

class SymbolTest : public ExactTest {
   protected:
    void SetUp() {
        ExactTest::SetUp();
        x = std::make_shared<Symbol>("x");
        x1 = std::make_shared<Symbol>("x");
        y = std::make_shared<Symbol>("y");
    }

    SymbolPtr x;
    SymbolPtr x1;
    SymbolPtr y;
};

class AdditionTest : public SymbolTest {
   protected:
    void SetUp() {
        SymbolTest::SetUp();
        a_e0_e1 = std::make_shared<Addition>(*e_0, *e_1);
        a_e20_e20 = std::make_shared<Addition>(*e_20, *e_20);
        a_x_e20 = std::make_shared<Addition>(*x, *e_20);
        a_x_y = std::make_shared<Addition>(*x, *y);
    }

    AdditionPtr a_e0_e1;
    AdditionPtr a_e20_e20;
    AdditionPtr a_x_e20;
    AdditionPtr a_x_y;
};

class SubtractionTest : public SymbolTest {
   protected:
    void SetUp() {
        SymbolTest::SetUp();
        s_e0_e1 = std::make_shared<Subtraction>(*e_0, *e_1);
        s_e20_e20 = std::make_shared<Subtraction>(*e_20, *e_20);
        s_x_e20 = std::make_shared<Subtraction>(*x, *e_20);
        s_x_y = std::make_shared<Subtraction>(*x, *y);
    }

    SubtractionPtr s_e0_e1;
    SubtractionPtr s_e20_e20;
    SubtractionPtr s_x_e20;
    SubtractionPtr s_x_y;
};

class MinusTest : public SymbolTest {
   protected:
    void SetUp() {
        SymbolTest::SetUp();
        m_e0 = std::make_shared<Minus>(*e_0);
        m_e20 = std::make_shared<Minus>(*e_20);
        m_x = std::make_shared<Minus>(*x);
    }

    MinusPtr m_e0;
    MinusPtr m_e20;
    MinusPtr m_x;
};

class MultiplicationTest : public SymbolTest {
   protected:
    void SetUp() {
        SymbolTest::SetUp();
        m_e0_e1 = std::make_shared<Multiplication>(*e_0, *e_1);
        m_e20_e20 = std::make_shared<Multiplication>(*e_20, *e_20);
        m_x_e20 = std::make_shared<Multiplication>(*x, *e_20);
        m_x_y = std::make_shared<Multiplication>(*x, *y);
    }

    MultiplicationPtr m_e0_e1;
    MultiplicationPtr m_e20_e20;
    MultiplicationPtr m_x_e20;
    MultiplicationPtr m_x_y;
};

class ResultTest : public AdditionTest {
   protected:
    void SetUp() {
        AdditionTest::SetUp();
        res_a_x_y = std::make_shared<Result>(*a_x_y);
    }
    ResultPtr res_a_x_y;
};

class SumTest : public AdditionTest {
    // TODO subtraction
   protected:
    void SetUp() override {
        AdditionTest::SetUp();
        std::vector<ExpressionPtr> s_0 = {a_x_y, x, Multiplication(*x, *y).to_ptr()};
        sum_0 = std::make_shared<Sum>(s_0);
        std::vector<ExpressionPtr> s_1 = {a_x_y, Multiplication(*a_x_y, *a_x_y).to_ptr()};
        sum_1 = std::make_shared<Sum>(s_1);
        std::vector<ExpressionPtr> s_2 = {x, y, x, y, Addition(Multiplication(*x, *y), *x).to_ptr()};
        sum_2 = std::make_shared<Sum>(s_2);
    }

    SumPtr sum_0;
    SumPtr sum_1;
    SumPtr sum_2;
};

class CombinedExpressionTest : public SymbolTest {
    //TODO unaray minus
   protected:
    void SetUp() override {
        SymbolTest::SetUp();
        comb_ex_0 = std::make_shared<Multiplication>(Addition(*x, *y), Addition(*y, *e_1));                 // (x+y)*(y+1)
        comb_ex_1 = std::make_shared<Multiplication>(Multiplication(*x, *y), Multiplication(*e_20, *e_1));  // (x*y*20*1)
        comb_ex_2 = std::make_shared<Multiplication>(Addition(*x, *y), Multiplication(*e_20, *e_20));       // (x+y)*(20*20)
    }

    MultiplicationPtr comb_ex_0;
    MultiplicationPtr comb_ex_1;
    MultiplicationPtr comb_ex_2;
};