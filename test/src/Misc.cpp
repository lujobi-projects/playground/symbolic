#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST_F(AdditionTest, AtomicClass) {
    Atomic& t = *e_0;
    EXPECT_EQ(t, *e_0);
}

TEST_F(ResultTest, ExtractResult) {
    EXPECT_EQ(*a_x_y, res_a_x_y->extract_result());
    EXPECT_EQ(*a_x_y, a_x_y->extract_result());
    EXPECT_FALSE(res_a_x_y->equals(*a_x_y, true));
    EXPECT_FALSE(a_x_y->equals(*res_a_x_y, true));
}

TEST(Expression_4, Copy_contsructor) {
    Exact e0(0);
    Exact e01 = e0;
    EXPECT_NE(&e0, &e01);
    EXPECT_EQ(e0, e01);
}

TEST(Expression_5, Copy_contsructor) {
    Exact e0(0);
    EXPECT_TRUE(e0.cast_ptr<Addition>());
}
