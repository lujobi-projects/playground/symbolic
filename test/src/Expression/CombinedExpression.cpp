#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST_F(CombinedExpressionTest, equality) {
    EXPECT_TRUE(comb_ex_0->equals(*comb_ex_0->to_ptr()));
    EXPECT_TRUE(comb_ex_1->equals(*comb_ex_1->to_ptr()));
    EXPECT_FALSE(comb_ex_1->equals(*comb_ex_2->to_ptr()));
    EXPECT_FALSE(comb_ex_2->equals(*comb_ex_1->to_ptr()));
}

TEST_F(CombinedExpressionTest, equality_exact) {
    EXPECT_TRUE(comb_ex_0->equals(*comb_ex_0, true));
    EXPECT_TRUE(comb_ex_1->equals(*comb_ex_1, true));
    EXPECT_FALSE(comb_ex_1->equals(*comb_ex_2, true));
    EXPECT_FALSE(comb_ex_2->equals(*comb_ex_1, true));
}
#include <iostream>
TEST_F(CombinedExpressionTest, differentiate) {
    Addition comb_ex_0_res = Addition(*y, *e_1);
    Result comb_ex_0_res_2 = (*y + *e_1) + (*x + *y);
    Multiplication comb_ex_1_res_x = Multiplication(*y, *e_20);
    Multiplication comb_ex_1_res_y = Multiplication(*x, *e_20);
    Exact comb_ex_2_res = Exact(400);

    EXPECT_EQ(simplify(*comb_ex_0->differentiate(*x)), comb_ex_0_res);
    EXPECT_EQ(simplify(*comb_ex_0->differentiate(*y)), comb_ex_0_res_2);
    EXPECT_EQ(simplify(*comb_ex_1->differentiate(*x)), comb_ex_1_res_x);
    EXPECT_EQ(simplify(*comb_ex_1->differentiate(*y)), comb_ex_1_res_y);
    EXPECT_EQ(simplify(*comb_ex_2->differentiate(*x)), comb_ex_2_res);
}

TEST_F(CombinedExpressionTest, to_string) {
    EXPECT_STREQ(comb_ex_0->to_string().c_str(), "(x + y) * (y + 1)");
    EXPECT_STREQ(comb_ex_1->to_string().c_str(), "x * y * 20 * 1");
    EXPECT_STREQ(comb_ex_2->to_string().c_str(), "(x + y) * 20 * 20");
}

TEST_F(CombinedExpressionTest, to_string_latex) {
    EXPECT_STREQ(comb_ex_0->to_string(true).c_str(), "(x + y) * (y + 1)");
    EXPECT_STREQ(comb_ex_1->to_string(true).c_str(), "x * y * 20 * 1");
    EXPECT_STREQ(comb_ex_2->to_string(true).c_str(), "(x + y) * 20 * 20");
}

TEST_F(CombinedExpressionTest, to_export_string) {
    EXPECT_STREQ(comb_ex_0->to_export_string().c_str(), "Multiplication(Addition(Symbol(x), Symbol(y)), Addition(Symbol(y), Exact(1)))");
    EXPECT_STREQ(comb_ex_1->to_export_string().c_str(), "Multiplication(Multiplication(Symbol(x), Symbol(y)), Multiplication(Exact(20), Exact(1)))");
    EXPECT_STREQ(comb_ex_2->to_export_string().c_str(), "Multiplication(Addition(Symbol(x), Symbol(y)), Multiplication(Exact(20), Exact(20)))");
}

TEST_F(CombinedExpressionTest, simplify) {
    // TODO not correct
    Addition a_e0_x(*e_0, *x);
    Addition a_x_e0(*x, *e_0);
    EXPECT_EQ(*comb_ex_0->simplify(), (*x + *y) * (*y + *e_1));
    EXPECT_EQ(*comb_ex_1->simplify(), (*x * *y) * *e_20);
    EXPECT_EQ(*comb_ex_2->simplify(), (*x + *y) * Exact(400));
}

TEST_F(CombinedExpressionTest, expand) {
    // TODO not correct
    EXPECT_EQ(*comb_ex_0->expand(), *comb_ex_0);
    EXPECT_EQ(*comb_ex_1->expand(), *comb_ex_1);
    EXPECT_EQ(*comb_ex_2->expand(), *comb_ex_2);
}

TEST_F(CombinedExpressionTest, substitute) {
    EXPECT_EQ(simplify(*comb_ex_0->substitute(*x, *e_1)), (*e_1 + *y) * (*y + *e_1));
    EXPECT_EQ(simplify(*comb_ex_1->substitute(*x, *e_1)), *y * *e_20);
    EXPECT_EQ(simplify(*comb_ex_1->substitute(*y, *e_1)), *x * *e_20);
    EXPECT_EQ(simplify(*comb_ex_2->substitute(*x, *e_1)), (*e_1 + *y) * Exact(400));
}
