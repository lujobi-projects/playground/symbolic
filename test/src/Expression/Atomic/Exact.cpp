#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST_F(ExactTest, equality) {
    EXPECT_TRUE(e_0->equals(*e_0));
    EXPECT_TRUE(e_1->equals(*e_1));
    EXPECT_FALSE(e_0->equals(*e_1));
    EXPECT_FALSE(e_1->equals(*e_20));
}

TEST_F(ExactTest, equality_exact) {
    EXPECT_TRUE(e_0->equals(*e_0, true));
    EXPECT_TRUE(e_1->equals(*e_1, true));
    EXPECT_FALSE(e_0->equals(*e_1, true));
    EXPECT_FALSE(e_1->equals(*e_20, true));
}

TEST_F(ExactTest, differentiate) {
    Symbol x("x");
    EXPECT_EQ(*e_0->differentiate(x), *e_0);
    EXPECT_EQ(*e_1->differentiate(x), *e_0);
    EXPECT_EQ(*e_20->differentiate(x), *e_0);
}

TEST_F(ExactTest, to_string) {
    EXPECT_STREQ(e_0->to_string().c_str(), "0");
    EXPECT_STREQ(e_1->to_string().c_str(), "1");
    EXPECT_STREQ(e_20->to_string().c_str(), "20");
}

TEST_F(ExactTest, to_string_latex) {
    EXPECT_STREQ(e_0->to_string(true).c_str(), "0");
    EXPECT_STREQ(e_1->to_string(true).c_str(), "1");
    EXPECT_STREQ(e_20->to_string(true).c_str(), "20");
}

TEST_F(ExactTest, to_export_string) {
    EXPECT_STREQ(e_0->to_export_string().c_str(), "Exact(0)");
    EXPECT_STREQ(e_1->to_export_string().c_str(), "Exact(1)");
    EXPECT_STREQ(e_20->to_export_string().c_str(), "Exact(20)");
}

TEST_F(ExactTest, simplify) {
    EXPECT_EQ(*e_0->simplify(), *e_0);
    EXPECT_EQ(*e_1->simplify(), *e_1);
    EXPECT_EQ(*e_20->simplify(), *e_20);
}

TEST_F(ExactTest, expand) {
    EXPECT_EQ(*e_0->expand(), *e_0);
    EXPECT_EQ(*e_1->expand(), *e_1);
    EXPECT_EQ(*e_20->expand(), *e_20);
}

TEST_F(ExactTest, substitute) {
    Symbol x("x");
    EXPECT_EQ(*e_0->substitute(x, *e_1), *e_0);
    EXPECT_EQ(*e_1->substitute(x, *e_1), *e_1);
    EXPECT_EQ(*e_20->substitute(x, *e_1), *e_20);
}
