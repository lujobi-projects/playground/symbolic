#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST_F(SymbolTest, equality) {
    EXPECT_TRUE(x->equals(*x));
    EXPECT_TRUE(y->equals(*y));
    EXPECT_FALSE(x->equals(*y));
    EXPECT_FALSE(y->equals(*x));
}

TEST_F(SymbolTest, equality_exact) {
    EXPECT_TRUE(x->equals(*x, true));
    EXPECT_TRUE(y->equals(*y, true));
    EXPECT_FALSE(x->equals(*y, true));
    EXPECT_FALSE(y->equals(*x, true));
}

TEST_F(SymbolTest, differentiate) {
    EXPECT_EQ(*x->differentiate(*x), *e_1);
    EXPECT_EQ(*x->differentiate(*y), *e_0);
    EXPECT_EQ(*y->differentiate(*y), *e_1);
}

TEST_F(SymbolTest, to_string) {
    EXPECT_STREQ(x->to_string().c_str(), "x");
    EXPECT_STREQ(y->to_string().c_str(), "y");
}

TEST_F(SymbolTest, to_string_latex) {
    EXPECT_STREQ(x->to_string(true).c_str(), "x");
    EXPECT_STREQ(y->to_string(true).c_str(), "y");
}

TEST_F(SymbolTest, to_export_string) {
    EXPECT_STREQ(x->to_export_string().c_str(), "Symbol(x)");
    EXPECT_STREQ(y->to_export_string().c_str(), "Symbol(y)");
}

TEST_F(SymbolTest, simplify) {
    EXPECT_EQ(*x->simplify(), *x);
    EXPECT_EQ(*y->simplify(), *y);
}

TEST_F(SymbolTest, expand) {
    EXPECT_EQ(*x->expand(), *x);
    EXPECT_EQ(*y->expand(), *y);
}

TEST_F(SymbolTest, substitute) {
    EXPECT_EQ(*x->substitute(*x, *e_1), *e_1);
    EXPECT_EQ(*x->substitute(*x, *y), *y);
    EXPECT_EQ(*y->substitute(*x, *e_1), *y);
}
