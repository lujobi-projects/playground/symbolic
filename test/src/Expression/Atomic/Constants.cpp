#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST(ConstantsTest, equality) {
    EXPECT_TRUE(E.equals(E));
    EXPECT_TRUE(PI.equals(PI));
    EXPECT_FALSE(E.equals(PI));
    EXPECT_FALSE(PI.equals(E));
}

TEST(ConstantsTest, equality_exact) {
    EXPECT_TRUE(E.equals(E, true));
    EXPECT_TRUE(PI.equals(PI, true));
    EXPECT_FALSE(E.equals(PI, true));
    EXPECT_FALSE(PI.equals(E, true));
}

TEST(ConstantsTest, differentiate) {
    Symbol x("x");
    EXPECT_EQ(*E.differentiate(x), Exact(0));
    EXPECT_EQ(*PI.differentiate(x), Exact(0));
    EXPECT_EQ(*E.differentiate(x), Exact(0));
}

TEST(ConstantsTest, to_string) {
    EXPECT_STREQ(E.to_string().c_str(), "e");
    EXPECT_STREQ(PI.to_string().c_str(), "pi");
    EXPECT_STREQ(E.to_string().c_str(), "e");
}

TEST(ConstantsTest, to_string_latex) {
    EXPECT_STREQ(E.to_string(true).c_str(), "\\e");
    EXPECT_STREQ(PI.to_string(true).c_str(), "\\pi");
    EXPECT_STREQ(E.to_string(true).c_str(), "\\e");
}

TEST(ConstantsTest, to_export_string) {
    EXPECT_STREQ(E.to_export_string().c_str(), "Constant(e)");
    EXPECT_STREQ(PI.to_export_string().c_str(), "Constant(pi)");
    EXPECT_STREQ(E.to_export_string().c_str(), "Constant(e)");
}

TEST(ConstantsTest, simplify) {
    EXPECT_EQ(*E.simplify(), E);
    EXPECT_EQ(*PI.simplify(), PI);
    EXPECT_EQ(*E.simplify(), E);
}

TEST(ConstantsTest, expand) {
    EXPECT_EQ(*E.expand(), E);
    EXPECT_EQ(*PI.expand(), PI);
    EXPECT_EQ(*E.expand(), E);
}

TEST(ConstantsTest, substitute) {
    Symbol x("x");
    EXPECT_EQ(*E.substitute(x, PI), E);
    EXPECT_EQ(*PI.substitute(x, PI), PI);
    EXPECT_EQ(*E.substitute(x, PI), E);
}
