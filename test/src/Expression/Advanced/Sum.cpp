#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST_F(SumTest, equality) {
    EXPECT_TRUE(sum_0->equals(*sum_0->to_ptr()));
    EXPECT_TRUE(sum_1->equals(*sum_1->to_ptr()));
    EXPECT_FALSE(sum_1->equals(*sum_2->to_ptr()));
    EXPECT_FALSE(sum_2->equals(*sum_1->to_ptr()));
}

TEST_F(SumTest, equality_exact) {
    EXPECT_TRUE(sum_0->equals(*sum_0, true));
    EXPECT_TRUE(sum_1->equals(*sum_1, true));
    EXPECT_FALSE(sum_1->equals(*sum_2, true));
    EXPECT_FALSE(sum_2->equals(*sum_1, true));
}
#include <iostream>
TEST_F(SumTest, differentiate) {
    Addition sum_0_res = Addition(*y, *e_1);
    Result sum_0_res_2 = (*y + *e_1) + (*x + *y);
    Multiplication sum_1_res_x = Multiplication(*y, *e_20);
    Multiplication sum_1_res_y = Multiplication(*x, *e_20);
    Exact sum_2_res = Exact(400);

    EXPECT_EQ(simplify(*sum_0->differentiate(*x)), sum_0_res);
    EXPECT_EQ(simplify(*sum_0->differentiate(*y)), sum_0_res_2);
    EXPECT_EQ(simplify(*sum_1->differentiate(*x)), sum_1_res_x);
    EXPECT_EQ(simplify(*sum_1->differentiate(*y)), sum_1_res_y);
    EXPECT_EQ(simplify(*sum_2->differentiate(*x)), sum_2_res);
}

TEST_F(SumTest, to_string) {
    EXPECT_STREQ(sum_0->to_string().c_str(), "x + y + x + x * y");
    EXPECT_STREQ(sum_1->to_string().c_str(), "x + y + (x + y) * (x + y)");
    EXPECT_STREQ(sum_2->to_string().c_str(), "x + y + x + y + x * y + x");
}

TEST_F(SumTest, to_string_latex) {
    EXPECT_STREQ(sum_0->to_string(true).c_str(), "x + y + x + x * y");
    EXPECT_STREQ(sum_1->to_string(true).c_str(), "x + y + (x + y) * (x + y)");
    EXPECT_STREQ(sum_2->to_string(true).c_str(), "x + y + x + y + x * y + x");
}

TEST_F(SumTest, to_export_string) {
    EXPECT_STREQ(sum_0->to_export_string().c_str(), "Sum(Addition(Symbol(x), Symbol(y)), Symbol(x), Multiplication(Symbol(x), Symbol(y)))");
    EXPECT_STREQ(sum_1->to_export_string().c_str(), "Sum(Addition(Symbol(x), Symbol(y)), Multiplication(Addition(Symbol(x), Symbol(y)), Addition(Symbol(x), Symbol(y))))");
    EXPECT_STREQ(sum_2->to_export_string().c_str(), "Sum(Symbol(x), Symbol(y), Symbol(x), Symbol(y), Addition(Multiplication(Symbol(x), Symbol(y)), Symbol(x)))");
}

TEST_F(SumTest, simplify) {
    // TODO not correct
    Addition a_e0_x(*e_0, *x);
    Addition a_x_e0(*x, *e_0);
    EXPECT_EQ(*sum_0->simplify(), (*x + *y) * (*y + *e_1));
    EXPECT_EQ(*sum_1->simplify(), (*x * *y) * *e_20);
    EXPECT_EQ(*sum_2->simplify(), (*x + *y) * Exact(400));
}

TEST_F(SumTest, expand) {
    // TODO not correct
    EXPECT_EQ(*sum_0->expand(), *sum_0);
    EXPECT_EQ(*sum_1->expand(), *sum_1);
    EXPECT_EQ(*sum_2->expand(), *sum_2);
}

TEST_F(SumTest, substitute) {
    EXPECT_EQ(simplify(*sum_0->substitute(*x, *e_1)), (*e_1 + *y) * (*y + *e_1));
    EXPECT_EQ(simplify(*sum_1->substitute(*x, *e_1)), *y * *e_20);
    EXPECT_EQ(simplify(*sum_1->substitute(*y, *e_1)), *x * *e_20);
    EXPECT_EQ(simplify(*sum_2->substitute(*x, *e_1)), (*e_1 + *y) * Exact(400));
}
