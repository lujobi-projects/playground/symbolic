#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST_F(ResultTest, equality) {
    // Expect two strings not to be equal.
    EXPECT_STRNE("hello", "world");
    // Expect equality.
    // EXPECT_TRUE(x == x1);
}

//TODO "UPGRADE" TO MORE COMPLEX RESULT
#include <iostream>
TEST_F(ResultTest, differentiate) {
    EXPECT_EQ(*res_a_x_y->differentiate(*x), Result(*a_x_y->differentiate(*x)));
}

TEST_F(ResultTest, to_string) {
    EXPECT_STREQ(res_a_x_y->to_string().c_str(), (a_x_y->to_string()).c_str());
}

TEST_F(ResultTest, to_string_latex) {
    EXPECT_STREQ(res_a_x_y->to_string(true).c_str(), a_x_y->to_string(true).c_str());
}

TEST_F(ResultTest, to_export_string) {
    EXPECT_STREQ(res_a_x_y->to_export_string().c_str(), ("Result(" + a_x_y->to_export_string() + ")").c_str());
}

TEST_F(ResultTest, simplify) {
    EXPECT_TRUE(*res_a_x_y->simplify() == Result(*a_x_y->simplify()));
}

TEST_F(ResultTest, expand) {
    EXPECT_TRUE(*res_a_x_y->expand() == Result(*a_x_y->expand()));
}

TEST_F(ResultTest, substitute) {
    EXPECT_TRUE(*res_a_x_y->substitute(*x, *e_1) == Result(*a_x_y->substitute(*x, *e_1)));
}
