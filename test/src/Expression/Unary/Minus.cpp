#include <iostream>

#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST_F(MinusTest, equality) {
    EXPECT_TRUE(m_e0->equals(*m_e0));
    EXPECT_TRUE(m_e20->equals(*m_e20));
    EXPECT_FALSE(m_x->equals(*m_e20));
}

TEST_F(MinusTest, equality_exact) {
    EXPECT_TRUE(m_e0->equals(*m_e0, true));
    EXPECT_TRUE(m_e20->equals(*m_e20, true));
    EXPECT_FALSE(m_x->equals(*m_e20, true));
    EXPECT_FALSE(m_e20->equals(*m_x, true));
}

TEST_F(MinusTest, differentiate) {
    EXPECT_EQ(*m_e0->differentiate(*x), *e_0);
    EXPECT_EQ(*m_x->differentiate(*x), Minus(Exact(1)));
    EXPECT_EQ(*m_x->differentiate(*y), Exact(0));
    EXPECT_EQ(*m_e20->differentiate(*x), *e_0);
}

TEST_F(MinusTest, to_string) {
    EXPECT_STREQ(m_e0->to_string().c_str(), "-0");
    EXPECT_STREQ(m_x->to_string().c_str(), "-x");
    EXPECT_STREQ(m_e20->to_string().c_str(), "-20");
}

TEST_F(MinusTest, to_string_latex) {
    EXPECT_STREQ(m_e0->to_string(true).c_str(), "-0");
    EXPECT_STREQ(m_x->to_string(true).c_str(), "-x");
    EXPECT_STREQ(m_e20->to_string(true).c_str(), "-20");
}

TEST_F(MinusTest, to_export_string) {
    EXPECT_STREQ(m_e0->to_export_string().c_str(), "Minus(Exact(0))");
    EXPECT_STREQ(m_x->to_export_string().c_str(), "Minus(Symbol(x))");
    EXPECT_STREQ(m_e20->to_export_string().c_str(), "Minus(Exact(20))");
}
#include <iostream>
TEST_F(MinusTest, simplify) {
    EXPECT_EQ(*m_e0->simplify(), Exact(0));
    EXPECT_EQ(*m_x->simplify(), Minus(*x));
    EXPECT_EQ(*m_e20->simplify(), Exact(-20));
}

TEST_F(MinusTest, expand) {
    // TODO not correct
    EXPECT_EQ(*m_e0->expand(), Exact(0));
    EXPECT_EQ(*m_x->expand(), Minus(*x));
    EXPECT_EQ(*m_e20->expand(), Exact(-20));
}

TEST_F(MinusTest, substitute) {
    EXPECT_EQ(*m_e0->substitute(*x, *e_1), Minus(Exact(0)));
    EXPECT_EQ(*m_x->substitute(*x, *e_1), Minus(Exact(1)));
    EXPECT_EQ(*m_e20->substitute(*x, *e_1), Minus(Exact(20)));
}
