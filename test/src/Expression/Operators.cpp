#include <sstream>

#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

// TEST(Operators, Asterisk) {
//     Exact e0(0);
//     Exact e01(e0);
//     EXPECT_NE(&e0, &e01);
//     EXPECT_TRUE(e0 == e01);
// }

// TODO use better / more complicated test cases

TEST_F(AdditionTest, Plus) {
    a_e0_e1 = std::make_shared<Addition>(*e_0, *e_1);
    a_x_e20 = std::make_shared<Addition>(*x, *e_20);
    a_x_y = std::make_shared<Addition>(*x, *y);
    EXPECT_EQ(*a_x_y, *x + *y);
    EXPECT_EQ(*a_x_e20, *x + *e_20);
    EXPECT_EQ(*a_e0_e1, *e_0 + *e_1);
}

TEST_F(ResultTest, Equality) {
    EXPECT_TRUE(*res_a_x_y == *a_x_y);
    EXPECT_TRUE(*a_x_y == *a_x_y);
    EXPECT_FALSE(*a_x_y == *a_x_e20);
    EXPECT_FALSE(*a_x_e20 == *a_x_y);
}

TEST_F(ResultTest, Inequality) {
    EXPECT_FALSE(*res_a_x_y != *a_x_y);
    EXPECT_FALSE(*a_x_y != *a_x_y);
    EXPECT_TRUE(*a_x_y != *a_x_e20);
    EXPECT_TRUE(*a_x_e20 != *a_x_y);
}

TEST_F(ResultTest, Substitute) {
    EXPECT_EQ(*res_a_x_y->substitute(*x, *e_20), substitute(*res_a_x_y, *x, *e_20));
    EXPECT_EQ(*res_a_x_y->substitute(*x, *y), substitute(*res_a_x_y, *x, *y));
    EXPECT_EQ(*res_a_x_y->substitute(*y, *e_20), substitute(*res_a_x_y, *y, *e_20));
    EXPECT_EQ(*res_a_x_y->substitute(*y, *x), substitute(*res_a_x_y, *y, *x));
}

TEST_F(ResultTest, Diffenentiate) {
    EXPECT_EQ(*res_a_x_y->differentiate(*x), diffenentiate(*res_a_x_y, *x));
    EXPECT_EQ(*res_a_x_y->differentiate(*y), diffenentiate(*res_a_x_y, *y));
}

TEST_F(ResultTest, Output) {
    std::stringstream s0;
    s0 << *res_a_x_y;
    EXPECT_STREQ(s0.str().c_str(), res_a_x_y->to_string().c_str());
    std::stringstream s1;
    s1 << *a_x_e20;
    EXPECT_STREQ(s1.str().c_str(), a_x_e20->to_string().c_str());
}
