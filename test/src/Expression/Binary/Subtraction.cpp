#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST_F(SubtractionTest, equality) {
    EXPECT_TRUE(s_e0_e1->equals(*s_e0_e1));
    EXPECT_TRUE(s_x_y->equals(*s_x_y));
    EXPECT_FALSE(s_x_e20->equals(*s_x_y));
    EXPECT_FALSE(s_x_y->equals(*s_x_e20));
}

TEST_F(SubtractionTest, equality_exact) {
    EXPECT_TRUE(s_e0_e1->equals(*s_e0_e1, true));
    EXPECT_TRUE(s_x_y->equals(*s_x_y, true));
    EXPECT_FALSE(s_x_e20->equals(*s_x_y, true));
    EXPECT_FALSE(s_x_y->equals(*s_x_e20, true));
}

TEST_F(SubtractionTest, differentiate) {
    Subtraction s_e0_e1_res = Subtraction(*e_0, *e_0);
    Subtraction s_x_e20_res = Subtraction(*e_1, *e_0);
    Subtraction s_x_y_res_x = Subtraction(*e_1, *e_0);
    Subtraction s_x_y_res_y = Subtraction(*e_0, *e_1);

    EXPECT_EQ(*s_e0_e1->differentiate(*x), s_e0_e1_res);
    EXPECT_EQ(*s_x_e20->differentiate(*x), s_x_e20_res);
    EXPECT_EQ(*s_x_y->differentiate(*x), s_x_y_res_x);
    EXPECT_EQ(*s_x_y->differentiate(*y), s_x_y_res_y);
}

TEST_F(SubtractionTest, to_string) {
    EXPECT_STREQ(s_e0_e1->to_string().c_str(), "0 - 1");
    EXPECT_STREQ(s_x_e20->to_string().c_str(), "x - 20");
    EXPECT_STREQ(s_x_y->to_string().c_str(), "x - y");
}

TEST_F(SubtractionTest, to_string_latex) {
    EXPECT_STREQ(s_e0_e1->to_string(true).c_str(), "0 - 1");
    EXPECT_STREQ(s_x_e20->to_string(true).c_str(), "x - 20");
    EXPECT_STREQ(s_x_y->to_string(true).c_str(), "x - y");
}

TEST_F(SubtractionTest, to_export_string) {
    EXPECT_STREQ(s_e0_e1->to_export_string().c_str(), "Subtraction(Exact(0), Exact(1))");
    EXPECT_STREQ(s_x_e20->to_export_string().c_str(), "Subtraction(Symbol(x), Exact(20))");
    EXPECT_STREQ(s_x_y->to_export_string().c_str(), "Subtraction(Symbol(x), Symbol(y))");
}

TEST_F(SubtractionTest, simplify) {
    Addition a_e0_x(*e_0, *x);
    Addition a_x_e0(*x, *e_0);
    EXPECT_EQ(*s_e0_e1->simplify(), Minus(Exact(1)));
    EXPECT_EQ(*s_x_e20->simplify(), *s_x_e20);
    EXPECT_EQ(*s_x_y->simplify(), *s_x_y);
}

TEST_F(SubtractionTest, expand) {
    // TODO not correct
    EXPECT_EQ(*s_e0_e1->expand(), *s_e0_e1);
    EXPECT_EQ(*s_x_e20->expand(), *s_x_e20);
    EXPECT_EQ(*s_x_y->expand(), *s_x_y);
}

TEST_F(SubtractionTest, substitute) {
    Subtraction s_x_e20_res = Subtraction(*e_1, *e_20);
    Subtraction s_x_y_res_x = Subtraction(*e_1, *y);
    Subtraction s_x_y_res_y = Subtraction(*x, *e_1);

    EXPECT_EQ(*s_e0_e1->substitute(*x, *e_1), *s_e0_e1);
    EXPECT_EQ(*s_x_e20->substitute(*x, *e_1), s_x_e20_res);
    EXPECT_EQ(*s_x_y->substitute(*x, *e_1), s_x_y_res_x);
    EXPECT_EQ(*s_x_y->substitute(*y, *e_1), s_x_y_res_y);
}
