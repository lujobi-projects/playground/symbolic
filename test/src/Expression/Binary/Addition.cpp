#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST_F(AdditionTest, equality) {
    EXPECT_TRUE(a_e0_e1->equals(*a_e0_e1));
    EXPECT_TRUE(a_x_y->equals(*a_x_y));
    EXPECT_FALSE(a_x_e20->equals(*a_x_y));
    EXPECT_FALSE(a_x_y->equals(*a_x_e20));
}

TEST_F(AdditionTest, equality_exact) {
    EXPECT_TRUE(a_e0_e1->equals(*a_e0_e1, true));
    EXPECT_TRUE(a_x_y->equals(*a_x_y, true));
    EXPECT_FALSE(a_x_e20->equals(*a_x_y, true));
    EXPECT_FALSE(a_x_y->equals(*a_x_e20, true));
}

TEST_F(AdditionTest, differentiate) {
    Addition a_e0_e1_res = Addition(*e_0, *e_0);
    Addition a_x_e20_res = Addition(*e_1, *e_0);
    Addition a_x_y_res_x = Addition(*e_1, *e_0);
    Addition a_x_y_res_y = Addition(*e_0, *e_1);

    EXPECT_EQ(*a_e0_e1->differentiate(*x), a_e0_e1_res);
    EXPECT_EQ(*a_x_e20->differentiate(*x), a_x_e20_res);
    EXPECT_EQ(*a_x_y->differentiate(*x), a_x_y_res_x);
    EXPECT_EQ(*a_x_y->differentiate(*y), a_x_y_res_y);
}

TEST_F(AdditionTest, to_string) {
    EXPECT_STREQ(a_e0_e1->to_string().c_str(), "0 + 1");
    EXPECT_STREQ(a_x_e20->to_string().c_str(), "x + 20");
    EXPECT_STREQ(a_x_y->to_string().c_str(), "x + y");
}

TEST_F(AdditionTest, to_string_latex) {
    EXPECT_STREQ(a_e0_e1->to_string(true).c_str(), "0 + 1");
    EXPECT_STREQ(a_x_e20->to_string(true).c_str(), "x + 20");
    EXPECT_STREQ(a_x_y->to_string(true).c_str(), "x + y");
}

TEST_F(AdditionTest, to_export_string) {
    EXPECT_STREQ(a_e0_e1->to_export_string().c_str(), "Addition(Exact(0), Exact(1))");
    EXPECT_STREQ(a_x_e20->to_export_string().c_str(), "Addition(Symbol(x), Exact(20))");
    EXPECT_STREQ(a_x_y->to_export_string().c_str(), "Addition(Symbol(x), Symbol(y))");
}

TEST_F(AdditionTest, simplify) {
    Addition a_e0_x(*e_0, *x);
    Addition a_x_e0(*x, *e_0);
    EXPECT_EQ(*a_e0_e1->simplify(), Exact(1));
    EXPECT_EQ(*a_e20_e20->simplify(), Exact(40));
    EXPECT_EQ(*a_e0_x.simplify(), *x);
    EXPECT_EQ(*a_x_e0.simplify(), *x);
    EXPECT_EQ(*a_x_e20->simplify(), *a_x_e20);
    EXPECT_EQ(*a_x_y->simplify(), *a_x_y);
}

TEST_F(AdditionTest, expand) {
    // TODO not correct
    EXPECT_EQ(*a_e0_e1->expand(), *a_e0_e1);
    EXPECT_EQ(*a_x_e20->expand(), *a_x_e20);
    EXPECT_EQ(*a_x_y->expand(), *a_x_y);
}

TEST_F(AdditionTest, substitute) {
    Addition a_x_e20_res = Addition(*e_1, *e_20);
    Addition a_x_y_res_x = Addition(*e_1, *y);
    Addition a_x_y_res_y = Addition(*x, *e_1);

    EXPECT_EQ(*a_e0_e1->substitute(*x, *e_1), *a_e0_e1);
    EXPECT_EQ(*a_x_e20->substitute(*x, *e_1), a_x_e20_res);
    EXPECT_EQ(*a_x_y->substitute(*x, *e_1), a_x_y_res_x);
    EXPECT_EQ(*a_x_y->substitute(*y, *e_1), a_x_y_res_y);
}
