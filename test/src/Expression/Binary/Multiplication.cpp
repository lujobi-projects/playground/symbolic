#include <iostream>

#include "ExpressionTestFixture.hpp"
#include "Symbolic/Symbolic.hpp"

TEST_F(MultiplicationTest, equality) {
    EXPECT_TRUE(m_e0_e1->equals(*m_e0_e1));
    EXPECT_TRUE(m_x_y->equals(*m_x_y));
    EXPECT_FALSE(m_x_e20->equals(*m_x_y));
    EXPECT_FALSE(m_x_y->equals(*m_x_e20));
}

TEST_F(MultiplicationTest, equality_exact) {
    EXPECT_TRUE(m_e0_e1->equals(*m_e0_e1, true));
    EXPECT_TRUE(m_x_y->equals(*m_x_y, true));
    EXPECT_FALSE(m_x_e20->equals(*m_x_y, true));
    EXPECT_FALSE(m_x_y->equals(*m_x_e20, true));
}

TEST_F(MultiplicationTest, differentiate) {
    Addition m_e0_e1_res = Addition(Multiplication(*e_0, *e_1), Multiplication(*e_0, *e_0));
    Addition m_x_e20_res = Addition(Multiplication(*e_1, *e_20), Multiplication(*x, *e_0));
    Addition m_x_y_res_x = Addition(Multiplication(*e_1, *y), Multiplication(*x, *e_0));
    Addition m_x_y_res_y = Addition(Multiplication(*e_0, *y), Multiplication(*x, *e_1));

    EXPECT_EQ(*m_e0_e1->differentiate(*x), m_e0_e1_res);
    EXPECT_EQ(*m_x_e20->differentiate(*x), m_x_e20_res);
    EXPECT_EQ(*m_x_y->differentiate(*x), m_x_y_res_x);
    EXPECT_EQ(*m_x_y->differentiate(*y), m_x_y_res_y);
}

TEST_F(MultiplicationTest, to_string) {
    EXPECT_STREQ(m_e0_e1->to_string().c_str(), "0 * 1");
    EXPECT_STREQ(m_x_e20->to_string().c_str(), "x * 20");
    EXPECT_STREQ(m_x_y->to_string().c_str(), "x * y");
}

TEST_F(MultiplicationTest, to_string_latex) {
    EXPECT_STREQ(m_e0_e1->to_string(true).c_str(), "0 * 1");
    EXPECT_STREQ(m_x_e20->to_string(true).c_str(), "x * 20");
    EXPECT_STREQ(m_x_y->to_string(true).c_str(), "x * y");
}

TEST_F(MultiplicationTest, to_export_string) {
    EXPECT_STREQ(m_e0_e1->to_export_string().c_str(), "Multiplication(Exact(0), Exact(1))");
    EXPECT_STREQ(m_x_e20->to_export_string().c_str(), "Multiplication(Symbol(x), Exact(20))");
    EXPECT_STREQ(m_x_y->to_export_string().c_str(), "Multiplication(Symbol(x), Symbol(y))");
}

TEST_F(MultiplicationTest, simplify) {
    Multiplication m_e0_x(*e_0, *x);
    Multiplication m_e1_y(*e_1, *y);
    Multiplication m_y_e1(*y, *e_1);
    EXPECT_EQ(*m_e0_e1->simplify(), Exact(0));
    EXPECT_EQ(*m_e20_e20->simplify(), Exact(400));
    EXPECT_EQ(*m_e0_x.simplify(), *e_0);
    EXPECT_EQ(*m_e1_y.simplify(), *y);
    EXPECT_EQ(*m_y_e1.simplify(), *y);
    EXPECT_EQ(*m_x_e20->simplify(), *m_x_e20);
    EXPECT_EQ(*m_x_y->simplify(), *m_x_y);
}

TEST_F(MultiplicationTest, expand) {
    // TODO not correct
    EXPECT_EQ(*m_e0_e1->expand(), *m_e0_e1);
    EXPECT_EQ(*m_x_e20->expand(), *m_x_e20);
    EXPECT_EQ(*m_x_y->expand(), *m_x_y);
}

TEST_F(MultiplicationTest, substitute) {
    Multiplication m_x_e20_res = Multiplication(*e_1, *e_20);
    Multiplication m_x_y_res_x = Multiplication(*e_1, *y);
    Multiplication m_x_y_res_y = Multiplication(*x, *e_1);

    EXPECT_EQ(*m_e0_e1->substitute(*x, *e_1), *m_e0_e1);
    EXPECT_EQ(*m_x_e20->substitute(*x, *e_1), m_x_e20_res);
    EXPECT_EQ(*m_x_y->substitute(*x, *e_1), m_x_y_res_x);
    EXPECT_EQ(*m_x_y->substitute(*y, *e_1), m_x_y_res_y);
}
